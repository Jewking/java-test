package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder { 
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        try {
            if(inputNumbers.stream().anyMatch(i -> i == null)) throw new CannotBuildPyramidException();
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError ex) {
            throw new CannotBuildPyramidException();
        }
        
        int n = inputNumbers.size();
        int idx = 0;
        int rows = 0;
        
        while(idx < n) {        
            rows++;                                                         // ����� �����
            for(int column=0; column < rows; column++) idx++; 
        }
        
        if(!CheckOpportunityBuildPyramid(rows, n)) throw new CannotBuildPyramidException();

        int columns = 2*rows - 1;                                           // ����� ��������
        idx = 0;
        
        int[][] result = new int[rows][columns];
        
        for(int i = 1; i <= rows; i++) {                    
            columns = 0;
            
            for(int j = 0; j < rows-i; j++) result[i-1][columns++] = 0;     // ����� ������
            
            for(int j = 0; j < i; j++) {
                result[i-1][columns++] = inputNumbers.get(idx++);           // ���������� �������
                if(idx >= n) break;                                         // �������� �� ��������� ����� (��� �������)
                else result[i-1][columns++] = 0;                            // ���������� �������
            }

            for(int j = 1; j < rows-i; j++)  result[i-1][columns++] = 0;    // ������ ������
        }
        return result;
    }

    public static boolean CheckOpportunityBuildPyramid(int rows, int n) {
        // row      col     row*col     n   difference
        // 1 -      1       -           -       -
        // 2 -      3       6           3       -
        // 3 -      5       15          6       3 (between 2 & 3)
        // 4 -      7       28          10      4 (between 3 & 4)
        // 5 -      9       45          15      5 (between 4 & 5)
        // 6 -      11      66          21      6 (between 5 & 6)
        //  ...

        if(n < 3)       return false;
        else if(n == 3) return true;
        else {
            int check_n = 3;
            for(int i = 3; i <= rows; i++) check_n += i;
            
            if(check_n == n)    return true;
            else                return false;
        }
    }
}
