package com.tsystems.javaschool.tasks.calculator;

import javax.script.*;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as
     *                  decimal mark, parentheses, operations signs '+', '-', '*',
     *                  '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is
     *         invalid
     */
    public String evaluate(String statement) {
        if(statement == null || statement.isEmpty()) return null;
        if(!checkStatemnentSyntax(statement))  return null;
        
        ScriptEngineManager mgr = new ScriptEngineManager();    
        ScriptEngine engine = mgr.getEngineByName("JS");     
        
        try { 
            Object result;
            result = (Object) engine.eval(statement);
            if(String.valueOf(result).equals("Infinity")) return null;
            return String.valueOf(result);
        } catch (ScriptException ex) {
            return null;
        }
    }
    
    public static boolean checkStatemnentSyntax(String statement) {
        String operands[]=statement.split("(\\d+\\.?\\d*)|\\)|\\(");
        for(int i = 0; i < operands.length; i++) {
            if(operands[i].equals("/") || operands[i].equals("*") || operands[i].equals("+") || operands[i].equals("-") || operands[i].isEmpty() || operands[i] == null) continue;
            return false;
        }
        return true;
    }
}
