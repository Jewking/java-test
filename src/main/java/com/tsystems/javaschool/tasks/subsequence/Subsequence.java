package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

public class Subsequence { 

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException
    {
        if(x == null || y == null)  throw new IllegalArgumentException();
        if(x.isEmpty())             return true;
	    else if(y.isEmpty())        return false;
	    
        int idx = 0;
	    for(int j = 0; j < y.size(); j++) {
            if(y.get(j).equals(x.get(idx))) {
                if(idx < x.size()-1) idx++;
            } else y.remove(j--);
            
	    }
        
        if(x.equals(y)) return true;
        else            return false;
    }
}